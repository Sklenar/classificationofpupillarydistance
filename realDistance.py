#Soubor pro prepocet pixelove vzdalenosti na realnou, pomoci vzdalenosti od objektivu a vzorce z dokumentace.

def calculateRealDistance(focaLength, realDistance, sensorHeight, imageWidth, distancePixels):
    #vsechny udaje jsou v mm a vysledek take, je tedy nutne jej na zaver prevest na cm => vydelit 10

    #print(realDistance,distancePixels,sensorHeight,focaLength,imageWidth)

    ret=(realDistance*distancePixels*sensorHeight)/(focaLength*imageWidth)

    return ret/10
#--------------------------------------------------------------------------------------------------------------------------