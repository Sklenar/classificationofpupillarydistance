#Soubor pro roztrideni vzdalenosti pomoci clusterizace

def cluster(data, maxgap):
    #serazeni vzdalenosti od nejmensi po nejvetsi
    data.sort(key=lambda x: x[1])
    #umisteni prvni vzdalenosti
    groups = [[data[0]]]
    i=0

    #zaokrouhleni podle maxGap aby nevznikali chyby kvuli jinemu poctu desetinnych mist maxGap a vzdalenosti
    decPlaces = len(str(maxgap).split('.')[1])+1

    #prochazeni vsech vzdalenosti a jejich postupne roztrideni
    for x in data[1:]:
        ref=groups[i][0][1]

        diff=round(abs(x[1] - ref),decPlaces)

        if diff <= maxgap:
            #zarazeni do stejneho clusteru jako aktualni
            groups[i].append(x)
        else:
            #vytvoreni noveho clusteru a posunuti na ni, dalsi vzdalenosti se budou zarazovat do ni, pokud tedy nejsou delsi nez maxGap
            i+=1
            groups.append([x])

    return groups
#--------------------------------------------------------------------------------------------------------------------------