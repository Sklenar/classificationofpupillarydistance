#Soubor pro detekci referencniho objektu - ctverce ve fotografii. Slouzi pro prepocitani pixelove vzdalenosti na realnou.

import cv2
import numpy as np

#vraci pomer realne velikosti strany ctverce a pixelove velikosti strany ctverce, take vraci fotku se zaznamenanym refObjektem pro argument show
def getMeasure(image,heightCM,show):
    #nalezeni ctvercu ve fotografii
    sq=find_squares(image)

    heightReff = 0

    #vyber ctverce
    if(len(sq)>2):
        k = len(sq) - 2
    else:
        if(len(sq)==0):
            return 0,image

        k = len(sq) - 1
    #vyber bodu ctverce mezi kterymi se vypocte pixelova velikost hrany
    if not((sq[k][0][1]-sq[k][2][1])<=7):
        heightReff = abs(sq[k][0][1] - sq[k][2][1])
    elif not((sq[k][0][0] - sq[k][2][0])<=7):
        heightReff = abs(sq[k][0][0] - sq[k][2][0])
    elif not ((sq[k][1][1] - sq[k][3][1]) <= 7):
        heightReff = abs(sq[k][1][1] - sq[k][3][1])
    elif not ((sq[k][1][0] - sq[k][3][0]) <= 7):
        heightReff = abs(sq[k][1][0] - sq[k][3][0])
    elif not ((sq[k][1][0] - sq[k][2][0]) <= 7):
        heightReff = abs(sq[k][1][0] - sq[k][2][0])
    elif not ((sq[k][1][1] - sq[k][2][1]) <= 7):
        heightReff = abs(sq[k][1][1] - sq[k][2][1])

    if(show):
        cv2.drawContours(image, sq, -1, (0, 255, 0), 3)

        cv2.circle(image, (sq[k][0][0], sq[k][0][1]), 5, (255, 0, 0), -1)
        cv2.circle(image, (sq[k][1][0], sq[k][1][1]), 5, (255, 0, 0), -1)
        cv2.circle(image, (sq[k][2][0], sq[k][2][1]), 5, (255, 0, 0), -1)
        cv2.circle(image, (sq[k][3][0], sq[k][3][1]), 5, (255, 0, 0), -1)

    #pokud je vracena nula tak funkce ktera vola tuto funkci vypise chybu ze refObjekt nebyl nalezen
    if(heightReff == 0):
        return 0, image
    else:
        return heightCM/heightReff,image
#--------------------------------------------------------------------------------------------------------------------------
#vypocet uhlu pro overeni ctverce
def angle_cos(p0, p1, p2):
    d1, d2 = (p0-p1).astype('float'), (p2-p1).astype('float')

    return abs( np.dot(d1, d2) / np.sqrt( np.dot(d1, d1)*np.dot(d2, d2) ) )
#--------------------------------------------------------------------------------------------------------------------------
#funkce pro nalezeni ctvercu ve fotografii
def find_squares(img):
    img = cv2.GaussianBlur(img, (5, 5), 0)
    squares = []
    heightMax, widthMax, channels = img.shape

    for gray in cv2.split(img):
        for thrs in range(0, 255, 26):
            if(thrs == 0):
                #prevod na obrysy
                bin = cv2.Canny(gray, 0, 50, apertureSize=5)
                bin = cv2.dilate(bin, None)
            else:
                _retval, bin = cv2.threshold(gray, thrs, 255, cv2.THRESH_BINARY)
            bin, contours, _hierarchy = cv2.findContours(bin, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
            for cnt in contours:
                cnt_len = cv2.arcLength(cnt, True)
                cnt = cv2.approxPolyDP(cnt, 0.15*cnt_len, True)
                #filtrovani nalezenych ctvercu
                if len(cnt) == 4 and cv2.contourArea(cnt) > 1000 and cv2.contourArea(cnt)<((heightMax*widthMax)-30000) and cv2.isContourConvex(cnt):
                    cnt = cnt.reshape(-1, 2)
                    max_cos = np.max([angle_cos( cnt[i], cnt[(i+1) % 4], cnt[(i+2) % 4] ) for i in range(4)])

                    if(max_cos < 0.1):
                        squares.append(cnt)
    return squares