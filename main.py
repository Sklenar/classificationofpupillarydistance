#Hlavni soubor pro spusteni aplikace. Zpracovani argumentu, nacteni fotografii, zavolani vypoctu zornic a vypis vysledku.

import glob
import argparse

import cv2
import dlib
import os
import json

from detectPuppils import getPupils
from detectRefObject import getMeasure
from realDistance import calculateRealDistance
from getInfMetadata import getInfAboutPhoto
from clusterizeDistances import cluster

#zpracovani argumentu
parser = argparse.ArgumentParser(description='Process some integers.')

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = False, help = "Path to the image.")
ap.add_argument("-s", "--show", required = False, help = "Show detections.")
ap.add_argument("-if", "--imageFolder", required = False, help = "Path to the folder with images.")
ap.add_argument("-ro", "--refObject", required = False, help = "Real size of one side in reference square.")
ap.add_argument("-rd", "--realDistance", required = False, help = "Real distance of person from camera lens [m].")
ap.add_argument("-mg", "--maxGap", required = False, help = "Difference between classes of pupillary distances.")
ap.add_argument("-jf", "--jsonFile", required = False, help = "Difference between classes of pupillary distances.")

args = vars(ap.parse_args())

shapePredPath="./shape_predictor_68_face_landmarks.dat"
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(shapePredPath)

distances=[]
realDistances=[]
errors=[]
images=[]
imageVst = args["image"]
refObj = args["refObject"]
realDistance = args["realDistance"]
imageFolder = args["imageFolder"]
show = args["show"]
jsonFile = args["jsonFile"]
maxGap = args["maxGap"]

resetFocalSensor = True

#show=True

if(maxGap):
    maxGap = float(maxGap)


focaLength = None
sensorHeight = None

#osetreni ruznych neplatnych kombinaci argumentu
if not((realDistance or refObj or refObj)):
	parser.error("Jeden z techto argumentu je povinny: realDistance nebo refObj nebo jsonFile.")
	exit(4)

if(imageFolder and not maxGap):
    parser.error("V pripade zadani vice fotografii je potreba zadat prepinac -mg kvuli roztrideni.")
    exit(4)

#nacteni inf souboru
if(jsonFile):
    jsonFile = json.loads(jsonFile)

    try:
        refObj = jsonFile['refObj']
        realDistance=jsonFile['realDistance']
    except Exception as e:
        print("V info souboru neco chybi nebo ma spatny format - meritkova cast.\n"+str(e))

    if(realDistance):
        try:
            focaLength = jsonFile['focaLength']
            sensorHeight = jsonFile['sensorHeight']
            resetFocalSensor = False
        except Exception as e:
            print("V info souboru neco chybi nebo ma spatny format - chybi udaje k prevedeni realne vzdalenosti, pokusim se je ziskat z metadat.\n"+str(e))
else:
    if(realDistance):
        realDistance=float(realDistance)
    else:
        refObj=float(refObj)

#nacteni fotografie
if(imageVst):
    im = cv2.imread(imageVst)
    #pokud existuje tak ji pridam do nactenych fotografii
    if not (im is None):images.append([imageVst,im])

#nacteni slozky (sady) fotografii
if(imageFolder):
    #vyhledani jpg souboru ve slozce
    for filename in glob.glob(imageFolder+"/*.jpg"):
        im=cv2.imread(filename)
        #pokud existuje tak ji pridam do nactenych fotografii
        if not (im is None):images.append([filename, im])

    for filename in glob.glob(imageFolder+"/*.jpeg"):
        im=cv2.imread(filename)
        if not (im is None):images.append([filename,im])

    for filename in glob.glob(imageFolder+"/*.png"):
        im=cv2.imread(filename)
        if not(im is None):images.append([filename,im])

#pokud je co zpracovavat tak zacinam vyhledavat stredy zornic a pocitat
if(len(images) and (realDistance or refObj)):
    #prochazeni vsech zadanych fotografii
    for fm, image in images:
        distances = []
        measure = 0
        y = 1

        #ziskani stredu zornic vsech tvari na fotografii a upravene fotografie kde jsou zanzmaenay pro pripad show prepinace
        pupils, im = getPupils(image, detector, predictor, show)

        #nenalezeni zadnych stredu zornic / oci ve fotografii
        if(len(pupils) == 0):
            errors.append([os.path.basename(fm), "Nebyl nalezen zadny oblicej ve fotografii."])

        #pro kazdou sadu dvou stredu zornic jedne osoby vypocti jejich pixelovou vzdalensot
        for person in pupils:
            #dist = math.hypot(person[1][0] - person[0][0], person[1][1] - person[0][1])
            dist=person[1][0] - person[0][0]
            distances.append(dist)

        #prepocet pixelove vzdalenosti na realnou pomoci detekce referencniho objektu - ctverce
        if(refObj):
            measure, im = getMeasure(im, refObj, show)

            if (measure == 0 or len(distances) == 0):
                errors.append([os.path.basename(fm),"Nebyl nalezen referencni objekt ve fotografii."])
            else:
                for d in distances:
                    d *= measure
                    #zapis realne vzdalenosti osoby k ostatnim
                    realDistances.append([os.path.basename(fm)+", osoba"+str(y),round(d,2)])
                    y += 1
        # prepocet pixelove vzdalenosti na realnou pomoci vzorce
        elif(realDistance):
            if not(focaLength and sensorHeight):
                focaLength, sensorHeight = getInfAboutPhoto(fm)

            if (not(focaLength) or not(sensorHeight)):
                errors.append([os.path.basename(fm),"Nebyly nalezeny potrebne udaje v metadatech."])

            else:
                imageHeight, imageWidth, channels = image.shape
                y = 1
                for d in distances:
                    v=calculateRealDistance(focaLength, realDistance*1000, sensorHeight, imageWidth, d)
                    #zapis realne vzdalenosti osoby k ostatnim
                    realDistances.append([os.path.basename(fm)+", osoba"+str(y),round(v,2)])
                    y += 1

        #ukazka zpracovane footgrafie pokud je to pozadovano
        if(show):
            cv2.imshow(fm, im)
            cv2.waitKey(0)

        #pokud nebyly zadany gobalne udaje pro prevod na realny rozmer tak je vyresetuj pro dalsi footgrafii
        if(resetFocalSensor):
            focaLength = None
            sensorHeight = None
else:
    print("Nebyl zadan zadny zdroj nebo zdroj neexistuje. Nebo nebyl zadan dostatek informaci k vypoctu.")
    exit(1)

#vypsani zaznamenanych chybnych fotografii
if(len(errors)):
    print("Errors: ")

    for er in errors:
        print("\t"+er[0],er[1])

    print("---------------------------------------------------------------------------------------------------------------")

#pokud je co tridit tak roztridim
if(maxGap and len(realDistances)>1):
    groups=cluster(realDistances, maxGap)
else:
    #vypsani jedne fotografie
    for r in realDistances:
        print(str(r[0])+" - "+str(r[1]))
    exit(0)

#vypsani roztridenych skupin
i=1
for g in groups:
    print("Skupina cislo "+str(i)+":")

    for im in g:
        print("\t",im[0],im[1])
    i+=1
