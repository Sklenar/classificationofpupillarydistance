#Soubor pro ziskani informaci z metadat pro prepocet na realnou vzdalenost na zaklade vzorce.

from PIL import Image, ExifTags

#jiny fotak muze mit bohuzel jinaci key pro hledane hodnoty, pokusil jsem se zde obsahnout alespon dve nejcastejsi moznosti
def getInfAboutPhoto(image):
    sensorHeight = None
    focaLength = None

    img = Image.open(image)

    #extrakce metadat, pokud selze tak fotografie nema metadata a koncim s None a None, funkce ktera tuto fci vola si zapise chybu ze metadata chybi
    try:
        exif = { ExifTags.TAGS[k]: v for k, v in img._getexif().items() if k in ExifTags.TAGS }
    except:
        return focaLength,sensorHeight

    #extrakce konkretnich metadat
    try:
        sensorHeight=4.0994*round(((float(exif['ExifImageWidth']))/(float(list(exif['FocalPlaneXResolution'])[0]) / float(list(exif['FocalPlaneXResolution'])[1])))*25.4,2)
        focaLength=round(float(list(exif['FocalLength'])[0])/float(list(exif['FocalLength'])[1]),3)

    except:
        #zkusim druhou moznost key v metadatech
        pass

    #pokud prvni pripad nenasel zkusim dalsi
    if not(sensorHeight and focaLength):
        try:
            focaLength = float(exif['FocalLengthIn35mmFilm'])
            sensorHeight = 35
        except:
            sensorHeight = 0
            focaLength = 0
    else:
        return focaLength, sensorHeight


    return focaLength, sensorHeight
#--------------------------------------------------------------------------------------------------------------------------