#Soubor pro detekci zornicek ve fotografii.

from imutils import face_utils
import math
import numpy as np
import imutils
import cv2

#rozmery zvetseneho vyrezu oka
widthEyeResize=500
heightEyeResize=250

#funkce pro nalezeni zornice ve vyrezu oka z tvare
def findPuppil(img,coorXmin, coorXmax,coorYmin,coorYmax,side,show):
    xV=0.0
    yV=0.0
    kernel = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
    imageHeight, imageWidth, channels = img.shape
    xCenter=imageWidth/2
    yCenter = imageHeight / 2

    imgBase=img.copy()

    #metoda kruznic - viz doku
    img = increase_brightness(img, 10)
    img = cv2.filter2D(img, -1, kernel)
    img = cv2.medianBlur(img, 5)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    """
    cv2.imshow("circles", img)
    cv2.waitKey()
    """

    circles = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, 1, 20,
                               param1=50, param2=30, minRadius=20, maxRadius=150)

    if circles is not None:
        """
        for i in circles[0, :]:
            # draw the center of the circle
            cv2.circle(imgBase, (i[0], i[1]), 2, (0, 0, 255), 3)
        """
        xV,yV = getNearestCenter(circles, xCenter,yCenter)

    #pokud metoda kruznic nenasla nasleduje metoda odlesku
    if (xV == 0.0 and yV == 0.0):
        pom = []
        img = imgBase.copy()
        img = cv2.filter2D(img, -1, kernel)
        #img = cv2.medianBlur(img, 5)
        img = cv2.Canny(img, 85, 100)
        im, contours, hierarchy = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)


        for con in contours:
            approx = cv2.approxPolyDP(con, 0.01 * cv2.arcLength(con, True), True)
            area = cv2.contourArea(con)

            if ((len(approx) >= 12) and (len(approx) < 30) and (area > 1)):
                pom.append(con)

        pom = filterCountour(pom, xCenter, yCenter)
        if (len(pom)):
            """
            cv2.drawContours(imgBase, pom, -1, (255, 0, 0), 2)
            cv2.imshow('Objects Detected', imgBase)
            cv2.waitKey()
            """

            pom = sorted(pom, key=lambda x: cv2.contourArea(x))

            for p in pom:
                M = cv2.moments(p)

                xV += M['m10'] / M['m00']
                yV += M['m01'] / M['m00']

            xV /= len(pom)
            yV /= len(pom)

    #pokud jsem nasel stred zornice tak jej musim prevest do puvodni fotografie abych jej mohl pouzit
    if not(xV == 0.0 and yV == 0.0):
        if(show):
            #show eye and pupil
            cv2.circle(imgBase, (int(xV), int(yV)), 5, (0, 255, 0), -1)
            cv2.imshow("puppil" + side + "Eye", imgBase)
            cv2.waitKey()

        #prepocet ziskaneho stredu zornice
        xV = coorXmin + xV / (widthEyeResize / (coorXmax - coorXmin))
        yV = coorYmin + yV / (heightEyeResize / (coorYmax - coorYmin))

    return (xV,yV)
#--------------------------------------------------------------------------------------------------------------------------
#funkce pro vyber bodu z nekolika bodu, vybere ten ktery je nejblize zadanemu stredu
def getNearestCenter(circles,xCenter,yCenter):
    circles = np.uint16(np.around(circles))[0]

    xRet = 0
    yRet = 0
    pomDistance=10000000000000000

    for c in circles:
        x = c[0]
        y = c[1]

        #vypocet vzdalenosti mezi stredem a aktualnim bodem
        distance = math.hypot(x - xCenter, y - yCenter)

        #zjisteni zda mam novy njelepsi bod (je blize stredu nez vsechny predchozi)
        if(distance<pomDistance):
            pomDistance = distance
            xRet = x
            yRet = y

    return xRet, yRet
#--------------------------------------------------------------------------------------------------------------------------
#funkce pro filtrovani obrysu (contours) na zaklade vzdalenosti jejich stredu od zadaneho stredu a maximalni vzdalenosti od stredu
def filterCountour(countours, xCenter, yCenter, maxDistance=350):
    ret=[]

    for c in countours:
        M = cv2.moments(c)

        x = M['m10'] / M['m00']
        y = M['m01'] / M['m00']

        distance = math.hypot(x - xCenter, y - yCenter)

        if(distance < maxDistance):
            ret.append(c)

    return ret
#--------------------------------------------------------------------------------------------------------------------------
#3.metoda pro urceni stredu zornice, urci stred ze zadanych bodu oka => pri formatu pasovych fotografii se jedna o stred zornice
def centeroidnp(arr):
    length = arr.shape[0]
    sum_x = np.sum(arr[:, 0])
    sum_y = np.sum(arr[:, 1])
    return sum_x/length, sum_y/length
#--------------------------------------------------------------------------------------------------------------------------
#funkce pro zvyseni jasu fotografie
def increase_brightness(img, value=30):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    lim = 255 - value
    v[v > lim] = 255
    v[v <= lim] += value

    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    return img
#--------------------------------------------------------------------------------------------------------------------------
#ridici funkce pro ziskani stredu zornic obou oci
def getPupils(image, detector, predictor, show):
    ret=[]
    os=[]
    #prevod na pevnou veliskost a cernobilou
    image = imutils.resize(image, height=1024, width=768)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    #detekce tvari v cernobilem obrazku
    rects = detector(gray, 1)

    #pokud neni nalezena tvar ve fotografii je vraceno prazdne pole misto bodu zornic, funkce ktera vola tuto funkci si zapise chybu ze tvar nebyla nalezena ve fotografii
    if(len(rects) == 0):
        return [], image

    #prochazeni vsech detekovanych tvari ve fotografii
    for (i, rect) in enumerate(rects):
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)

        #extrakce oci
        lEye=face_utils.FACIAL_LANDMARKS_IDXS['left_eye']
        rEye= face_utils.FACIAL_LANDMARKS_IDXS['right_eye']

        #vytvoreni utvaru z bodu oci
        lEye=shape[lEye[0]:lEye[1]]
        rEye=shape[rEye[0]:rEye[1]]

        """
        print("left_eye:")
        for (x, y) in lEye:
            print("\t",x,y)
        """
        """
        print("right_eye:")
        for (x, y) in rEye:
           print("\t", x, y)
        """

        #vytvoreni vyrezu oci
        koef=1
        (x1, y1, w1, h1) = cv2.boundingRect(np.array([lEye]))
        roi = image[y1:y1 + int(h1/koef), x1:x1 + int(w1/koef)]
        roi = imutils.resize(roi, width=widthEyeResize, height=heightEyeResize,inter=cv2.INTER_CUBIC)

        (x2, y2, w2, h2) = cv2.boundingRect(np.array([rEye]))
        roi2 = image[y2:y2 + int(h2/koef), x2:x2 + int(w2/koef)]
        roi2 = imutils.resize(roi2, width=widthEyeResize, height=heightEyeResize, inter=cv2.INTER_CUBIC)

        #nalezeni stredu zornic
        leftEyePuppil=findPuppil(roi, lEye[0][0], lEye[3][0], lEye[1][1], lEye[5][1], "Left", show)
        rightEyePuppil=findPuppil(roi2, rEye[0][0], rEye[3][0], rEye[1][1], rEye[5][1], "Right", show)

        #pokud prvni a druha metoda pro nalezeni stredu zornic selzou je zavolana treti metoda
        if(leftEyePuppil==(0,0)):
            leftEyePuppil=centeroidnp(lEye)
        if (rightEyePuppil==(0, 0)):
            rightEyePuppil=centeroidnp(rEye)

        #zapsani nalezenych stredu osoby abych vedel ktere patri k dane osobe
        os.append(rightEyePuppil)
        os.append(leftEyePuppil)

        #pridani nalezenych zornic osoby k ostatnim osobam na fotografii
        ret.append(os)
        os=[]

        if(show):
            cv2.circle(image, tuple([int(leftEyePuppil[0]),int(leftEyePuppil[1])]), 7, (0, 255, 0), -1)
            cv2.circle(image, tuple([int(rightEyePuppil[0]),int(rightEyePuppil[1])]), 7, (0, 255, 0), -1)

    return ret, image
#--------------------------------------------------------------------------------------------------------------------------